package kafka

import (
	"context"
	"fmt"
	"log"

	"github.com/segmentio/kafka-go"
)

const (
	updateCurrencyTopic = "currency-update"
	defaultAddress      = "localhost:9092"
)

func NewConsumer() {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{defaultAddress},
		Topic:     updateCurrencyTopic,
		Partition: 0,
		MaxBytes:  10e6, // 10MB
	})

	defer r.Close()

	for {
		msg, err := r.ReadMessage(context.Background()) // fetch 10KB min, 1MB max
		if err != nil {
			log.Print(err)
			break
		}
		fmt.Println("MSG: ", string(msg.Value))
	}
}
