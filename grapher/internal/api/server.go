package api

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Server struct {
	router  *echo.Echo
	service Grapher
}

func SetupRoutes() {
	e := echo.New()
	currency := e.Group("/cryptocurrency")
	currency.GET("/price", priceHandler)
	e.Logger.Fatal(e.Start(":1323"))
}

func cryptocurrenciesHandler(c echo.Context) error {
	conn, err := upgrader.Upgrade(c.Response().Writer, c.Request(), nil)
	if err != nil {
		log.Println(err)
		return err
	}

	return c.String(http.StatusOK, "Hello, World!")
}
