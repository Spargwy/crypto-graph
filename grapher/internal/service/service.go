package service

type Service struct {
	Cache Cacher
}

func New(cacher Cacher) *Service {
	return &Service{
		Cache: cacher,
	}
}
