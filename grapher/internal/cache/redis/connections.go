package redis

import "context"

func (c *Cache) GetListenersForCryptoCurrency(ctx context.Context, currencies ...string) {
	for _, currency := range currencies {
		c.redisConn.Get(ctx, currency)
	}
}
