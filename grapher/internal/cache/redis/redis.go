package redis

import "github.com/redis/go-redis/v9"

type Cache struct {
	redisConn *redis.Client
}

func New() Cache {
	rdb := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})

	return Cache{redisConn: rdb}
}
