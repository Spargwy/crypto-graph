package main

import (
	"log"

	"github.com/Spargwy/crypto-wrapper/app"
	"github.com/Spargwy/crypto-wrapper/internal/external"
)

func main() {
	app.New()
	log.Print(external.FetchCurrencies())
}
