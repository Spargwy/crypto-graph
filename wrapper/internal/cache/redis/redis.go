package redis

import (
	"errors"

	"github.com/go-redis/redis"
)

type Cache struct {
	redisConn *redis.Client
}

func New() Cache {
	rdb := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})

	return Cache{redisConn: rdb}
}

func (c *Cache) AddCurrencyToRequested(currencies ...string) {
	for _, curr := range currencies {
		cu := c.redisConn.Get(curr)
		if cu.Err() != nil && errors.Is(cu.Err(), redis.Nil) {
			c.redisConn.Append("currencies", curr)
		}
	}
}
