package models

type Price struct {
	Title string
	Value float64
}

type CurrencyPrice struct {
	Title string
	Price []Price
}

type CurrencyMessage struct {
	CurrenciesPrice []CurrencyPrice
}
