package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/Spargwy/crypto-wrapper/internal/models"
	kafka "github.com/segmentio/kafka-go"
)

const (
	updateCurrencyTopic     = "currency-update"
	defaultAddress          = "localhost:9092"
	updateRequestedCurrency = "update-requested"
)

func ProduceCurrencyMsg(msgs models.CurrencyMessage) error {
	conn, err := kafka.DialLeader(context.Background(), "tcp", defaultAddress, updateCurrencyTopic, 0)
	if err != nil {
		log.Fatalf("failed to dial leader: %v", err)
	}

	msg, err := json.Marshal(msgs)
	if err != nil {
		return fmt.Errorf("marshal: %w", err)
	}

	conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	written, err := conn.WriteMessages(
		kafka.Message{Value: msg},
	)
	if err != nil {
		return fmt.Errorf("failed to write message: %v", err)
	}
	if written == 0 {
		return fmt.Errorf("no one byte was sent")
	}

	err = conn.Close()
	if err != nil {
		return fmt.Errorf("failed to close writer: %v", err)
	}

	return nil
}

func NewConsumer() error {
	r, err := kafka.DialLeader(context.Background(), "tcp", defaultAddress, updateRequestedCurrency, 0)
	if err != nil {
		return fmt.Errorf("create dialLeader")
	}

	for {
		msg, err := r.ReadMessage(10 << 6) // fetch 10KB min, 1MB max
		if err != nil {
			log.Print(err)
			break
		}
		fmt.Println("MSG: ", string(msg.Value))
	}

	return nil
}
