package mongo

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type DB struct {
	db *mongo.Client
}

func New() (*DB, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		return nil, fmt.Errorf("connect to mongo: %w", err)
	}

	return &DB{db: client}, nil
}

func (d *DB) Disconnect(ctx context.Context) error {
	return d.db.Disconnect(ctx)
}

func (d *DB) Ping(ctx context.Context) error {
	return d.db.Ping(ctx, readpref.Primary())
}

func (d *DB) GetCurrency(ctx context.Context) (string, error) {
	currencies := d.db.Database("test").Collection("currencies")
	res, err := currencies.InsertOne(ctx, bson.D{{Key: "name", Value: "pi"}, {Key: "value", Value: 3.14159}})
	if err != nil {
		return "0", err
	}

	return primitive.ObjectID.String(res.InsertedID.(primitive.ObjectID)), nil
}
