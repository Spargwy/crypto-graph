package external

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/Spargwy/crypto-wrapper/internal/kafka"
	"github.com/Spargwy/crypto-wrapper/internal/models"
)

const yourAPIKey = "ae0bbbd3-f367-466f-8282-d03156d90223"

func FetchCurrencies() error {
	url := "https://pro-api.coinmarketcap.com/v2/cryptocurrency/quotes/latest?id=1,1027"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return fmt.Errorf("build request: %w", err)
	}
	req.Header.Add("X-CMC_PRO_API_KEY", yourAPIKey)

	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("send request: %w", err)
	}
	defer res.Body.Close()

	currency := models.CurrenciesResponse{}
	err = json.NewDecoder(res.Body).Decode(&currency)
	if err != nil {
		return fmt.Errorf("decode response: %w", err)
	}

	msg := models.CurrencyMessage{}
	for title, cur := range currency.Data {
		price := models.CurrencyPrice{
			Title: title,
		}

		for curTitle, t := range cur.Quote {
			price.Price = append(price.Price, models.Price{
				Title: curTitle,
				Value: t.Price,
			})
		}

		msg.CurrenciesPrice = append(msg.CurrenciesPrice, price)
	}

	return kafka.ProduceCurrencyMsg(msg)
}
