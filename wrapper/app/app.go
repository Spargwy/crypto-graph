package app

import (
	"context"
	"log/slog"

	"github.com/Spargwy/crypto-wrapper/internal/db/mongo"
)

type app struct {
}

func New() *app {
	ctx := context.Background()
	mongoDB, err := mongo.New()
	if err != nil {
		slog.Error("init mongo DB: %v", err)
		panic(err)
	}

	defer func() {
		err := mongoDB.Disconnect(context.Background())
		if err != nil {
			slog.Error("disconnect: %v", err)
			panic(err)
		}
	}()

	err = mongoDB.Ping(ctx)
	if err != nil {
		slog.Error("ping: %v", err)
		panic(err)
	}

	id, err := mongoDB.GetCurrency(ctx)
	if err != nil {
		slog.Error("getCurrency", err)
		panic(err)
	}

	slog.Info("id", slog.String("id", id))
	return nil
}
